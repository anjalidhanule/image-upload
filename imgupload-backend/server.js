const mongoUrl = "mongodb+srv://admin:admin@cluster0-0zgss.mongodb.net/test?retryWrites=true",
    express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    fs = require('fs'),
    multer = require("multer");
    var upload = multer({ dest: 'upload/'});

app 
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(cors())

mongoose.connect(mongoUrl)
    .then(result => console.log("MongoDb connected"))
    .catch(error => console.log("error"))

app.listen(3000, () => console.log('server started on port 3000'))

const imageSchema = new mongoose.Schema({
    img:{data: Buffer, contentType: String}
});

const photos = mongoose.model('imageTable', imageSchema);

var type = upload.single('image');
app.post('/uploadImage', type,  async(req, res)=>{

	if(req.file.mimetype === 'image/jpg' || req.file.mimetype  === 'image/png' || req.file.mimetype  === 'image/jpeg'){
		var imagpath = req.file.path;
		var t = new photos;
		t.img.data = fs.readFileSync(imagpath);
		t.img.contentType = req.file.mimetype;
		t.save(function (err, a) {
	      	if (err) throw err;
	      	else res.json({message:'Image added to database'})
	      	 });
	}
	else{
		res.json({message:'Only jpg, jpeg, png extension accepted'})
	}
	
});


app.get('/find', type,  async(req, res)=>{
  	const result = await photos.findById('5cdab8e04f05af1bf86e892e');
  	var thumb = new Buffer(result.img.data).toString('base64');
  	res.send({'img':thumb})
  	//res.render('index', { title: 'Express', img: thumb});
});

 
    app.delete('/del', async (req, res) => {
        const result = await photos.remove()
        res.json(result);
    })