import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { splitClasses } from '@angular/compiler';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-imageupload';
  selectedfile: File=null;
  images: any = [];
  allfiles: any = [];
  img = null;

  constructor(private http: HttpClient){
    this.http.get('http://localhost:3000/find').subscribe(res=>{
    console.log(res);//base64 image 
    })
  }

  url = '';
  selectImage(event){
    this.selectedfile =<File> event.target.files[0];
    let files = event.target.files;
    console.log(files);
    if (files) {
      for (let i = 0; i < files.length; i++) {
        const image = {
          url: ''
        }
        console.log(files[i])
        this.allfiles.push(files[i]);
        const reader = new FileReader();
        reader.onload = (filedata) => {
          image.url = reader.result + '';
          this.images.push(image);
        };
        reader.readAsDataURL(files[i]);
      }
    }
  }

  onUpload(){
      const formdata = new FormData();
      formdata.append('image', this.selectedfile, this.selectedfile.name); //uploading single image at a time to database
        this.http.post('http://localhost:3000/uploadImage', formdata).subscribe(res => {
          console.log(res);
        })
      }
    }
  
